'use strict';

angular.module('tauLauncher')
  .controller('GamesController', GamesController);
  
  function GamesController(remoteService) {
    var vm = this;
    var recentMockList = [
        {
          'id': 1,
          'name': "Heresy",
          'players': 'Rvach, Andrew, Player1, Player2' 
        },
        {
          'id': 2,
          'name': "Boduhar",
          'players': 'Player1, Player2, Player3' 
        },
        {
          'id': 3,
          'name': "KarToP/\9",
          'players': 'Bulba1, Bulba2, Bulba3' 
        },
        {
          'id': 4,
          'name': "Warp",
          'players': 'Creature1, Creature2, Creature3' 
        }
      ]; 
    
    var listOfTournaments   = [],// remoteService.getTournamentsList(),
        listOfChampionships = [],// remoteService.getChampionshipsList(),
        listOfCustomGames   = [];// remoteService.getCustomGamesList();
    
    vm.getRecentTournaments = function() {
      return listOfTournaments;
    }
    vm.getRecentChampionships = function() {
      return listOfChampionships;
    }
    vm.getRecentCustomGames  = function() {
      return listOfCustomGames;
    }
  }
