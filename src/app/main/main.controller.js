(function() {
  'use strict';

  angular
    .module('tauLauncher')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, gameService,remoteService) 
  {
    var vm = this;
 
    vm.classAnimation = 'fadeIn';
    vm.creationDate = 1439934491096;


    activate();

    function activate() 
    {
      $timeout(function() {
        // see https://daneden.github.io/animate.css/
        vm.classAnimation = 'infinite pulse';
      }, 4000);
      // $timeout(function() {
      //   // see https://daneden.github.io/animate.css/
      //   vm.classAnimation = 'hidden';
      // }, 5000);
    }
    
    vm.startGame = function()
    {
      gameService.startGame();
    }

    vm.latestNews = [];

    remoteService.getNewsList().then(function(news){
      vm.latestNews = news.slice(news.length-1, news.length);[];
    });
  }
})();
