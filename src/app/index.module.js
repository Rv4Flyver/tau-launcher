(function() {
  'use strict';

  angular
    .module('tauLauncher', ['ngMessages', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr'], function($compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|chrome-extension):|data:image\//);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|chrome-extension):/);
    });

})();
