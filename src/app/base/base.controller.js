'use strict';

angular.module('tauLauncher')
  .controller('BaseController', BaseController);
  
  function BaseController($scope, $rootScope, $http, $log, $state, launcherService, remoteService, gameService, updaterService, ModalsService, UserService, messagingService) 
  {
    var vm = this;
    vm.updaterService = updaterService;
    vm.isChatOnline = false;
   //------------
    // vm.contentUpdateProgress = 0;
    // vm.isUpdaterWorking = false;
    updaterService.updateLauncher(
        function() {
                $log.log('Launcher Update Successfull!');
        },
        function() {
                $log.log('Launcher is up to date.');

                // Authorization Code
                if(UserService.authorize()) {
                    ModalsService.message("Welcome back " + UserService.getUserData().login + "!");
                    messagingService.startClient(UserService.getUserData().login);
                } else {
                    ModalsService.registerUser();
                }

                updaterService.updateGameContent(
                    function() {
                        $log.log('Game Data Update Successfull!');
                    },
                    function() {
                        $log.log('Game Data is up to date.');
                    }
                );
        }
    );
    // listen for updater service // TODO Refactor
    $rootScope.$on('UpdateProgressChanged', function(event, data) {
        vm.isUpdaterWorking = updaterService.isWorking;
        vm.contentUpdateProgress = updaterService.downloadProgress;
        vm.contentUpdateProgressShort = updaterService.downloadProgress | 0;
        if(!$scope.$$phase) {
          $scope.$apply();     
        } 
    });

    // listen for messaging service // TODO Refactor
    $rootScope.$on('IRC_NicknamesChanged', function(event, nicknames) {
        vm.friends = nicknames;
        vm.isChatOnline = true;
        if(!$scope.$$phase) {
          $scope.$apply();     
        } 
    });
    
    // $scope.$watch(function(){
    //     return updaterService.downloadProgress;
    // }, function (downloadProgress) {
    //     vm.contentUpdateProgress = downloadProgress;
    //     $log.log("downloadProgress " + downloadProgress);
    // }, true);
    //------------------
    if( $state.current.name == "base")
    {
      $state.go("base.main");
    }
    
    vm.bookmarks = [
      {"id":1, "name": "News","state": "base.news"},
      {"id":2, "name": "Create Game","state": "base.games.custom.create"},       
      {"id":3, "name": "Join Game","state": "base.games.custom.join"}, 
      {"id":4, "name": "Options","state": "base.options"}
    ];
    
    vm.friends = [];

    
    var teams = remoteService.getTeamsList();
    vm.teams = teams;
    
    var players = remoteService.getPlayersList();
    vm.players = players;
    
    // Getters
    vm.getUserData = function() {
      return UserService.getUserData();
    }

    // Shutdown the luncher
    vm.Close = function()
    { 
      var isRequestConfirmed = confirm("Are you sure you want to shutdown the launcher?");
      if (isRequestConfirmed)
      {
        launcherService.Close();       
      }
    }
    
    // Toggle Kiosk Mode
    vm.ToggleKioskMode = function()
    { 
      launcherService.ToggleKioskMode();
    }

    vm.launcher = {
        'isMiniMode' : true
    };

    vm.switchState = function(state) {
        $state.go(state);
    }


  }
