'use strict';

angular.module('tauLauncher')
  .controller('JoinCustomGameController', JoinCustomGameController);
  
  function JoinCustomGameController(gameService, remoteService,UtilsService) {
    var vm = this;
    
    vm.listOfCustomGames = [];
    vm.listOfFreeGames = [];

    remoteService.getCustomGamesList().then(function(customGamesList){
      vm.listOfCustomGames = customGamesList;
    }); 
    
    remoteService.getFreeGamesList().then(function(freeGamesList){
      vm.listOfFreeGames = freeGamesList;
    }); 

    vm.ip = "127.0.0.1";
    vm.port = 26000;
       
    vm.Join = function(ip,port) {
      UtilsService.net.testUDPPort(port,ip,function(result,e){
        if(result=='success'){
          gameService.joinCustomServer(ip,port);
        }
        else {
          alert("Server is unavaliable");
        }
      });
    }

    vm.TestAvaliability = function(ip,port) {
      UtilsService.net.testUDPPort(port,ip,function(result,e){
        if(result=='success'){
          alert("Server is avaliable");
        }
        else {
          alert("Server is unavaliable");
        }
      });
    }
  }
