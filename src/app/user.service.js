'use strict';

angular
    .module('tauLauncher')
    .service('UserService', UserService);

/** @ngInject */
function UserService($log,UtilsService,remoteService, rootDir, dataDir) 
{
    var dataDir  = UtilsService.resolvePath(rootDir, dataDir);
    var dataFile = UtilsService.resolvePath(dataDir,"private.json");
    var vm = this;
    
    var userData = {
        login: '',
        registrationDate: null,
        country: '',
        city: '',
        clan: 0
    };

    vm.getUserData = function() {
        return userData;
    }
    
    vm.checkIsRegistered = function() {
        return UtilsService.isFileExists(dataFile);
    }

    vm.register = function(userData) {
        var result = {
            code : 0,
            type: 'filesystem'
        };
        //try{
        return    remoteService.createPlayer(userData.login,"New Player created from launcher.").then(
                function(success) {
                    var mkdirp = require('mkdirp');
                    mkdirp(dataDir, function(err) {
                        if (err) 
                        {
                            $log.log("> Cannot create data directory.");
                            $log.error(err);
                        }
                        else {
                            $log.log('> Directory ' + dataDir + ' was created successfully.');
                            UtilsService.write2File(dataFile, angular.toJson(userData), 'utf8');
                        }
                    });
                    result.code = 0;
                    return result;
                },
                function(failure){
                    //defer.reject(reject.error);
                    $log.log('Player creation failure:' + failure.data.error.code + " " + failure.data.error.message);
                    result.code = -1;
                    result.message = failure.data.error.message;
                    return result;
                }
            );


        // }
        // catch(e) {
        //     $log.log(e);
        //     result.code = -1;
        // }

        // return result;
    }

    vm.authorize = function() {
        $log.log(" Check if registered:" + vm.checkIsRegistered());
        if(vm.checkIsRegistered()) {
            userData = readLocalData();
            return true;
        }
        return false;
    }

    function readLocalData() 
    {
        var localData = UtilsService.readJSONFile(dataFile);
        return localData;
    }

}