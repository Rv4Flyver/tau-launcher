  'use strict';

  angular
      .module('tauLauncher')
      .service('remoteService', remoteService);

  /** @ngInject */
  function remoteService($resource, remotePaths, $log,$q) 
  {
    var remoteService = this;
    
    // Custom Tournaments ---------------------------------------------------------------
    remoteService.getTournamentsList = function() 
    {
        var list = $resource(remotePaths.root+remotePaths.tournaments).query(function() {
            $log.log(list);
          });
        return list;
    }
    
    // Custom Championships ---------------------------------------------------------------
    remoteService.getChampionshipsList = function() 
    {
        var list = $resource(remotePaths.root+remotePaths.championships).query(function() {
            $log.log(list);
          });
        return list;
    }
    
    // Custom Games -----------------------------------------------------------------------
    remoteService.getCustomGamesList = function() 
    {
        var defer = $q.defer();

        var customGames = $resource(remotePaths.root+remotePaths.customGames);
        
        customGames.query(function(result) {
            $log.log('Cusom Games List:' + result);
            // if(result.length == 0){
            //     result.push({
            //         id: 0,
            //         ip: "195.123.209.124",
            //         name: "Test Server",
            //         players: "..."
            //     });
            // } //temporarily
            defer.resolve(result);
          });
        return defer.promise;
	}
    // Servers -----------------------------------------------------------------------
    remoteService.getFreeGamesList = function() 
    {
        var defer = $q.defer();
        var freeArenas = $resource(remotePaths.root+remotePaths.servers.arenas.free);
        
        freeArenas.query(function(result) {
            $log.log('Free Arenas List:' + result);
            defer.resolve(result);
          });
        return defer.promise;
	}
    // Players --------------------------------------------------------------------------
    remoteService.getPlayersList = function() 
    {
        var list = $resource(remotePaths.root+remotePaths.players.index).query(function() {
            $log.log(list);
          });
        return list;
	}

    remoteService.createPlayer = function(name,info) 
    {
        //var defer = $q.defer();

        var player = $resource(remotePaths.root+remotePaths.players.create,{});
        var params = {'name': name,
                      'info': info,
                      'map_id': 0,
                      'desired_team_id': 0 };
        return player.save(params, 
            function(result) {
                //defer.resolve(result[0]);
                $log.log('Player creation responce:' + result);
            },
            function(reject){
                //defer.reject(reject.error);
                $log.log('Player creation failure:' + reject.error);
            }
        ).$promise;
        //return defer.promise;
	}
    
    // Teams ----------------------------------------------------------------------------
    remoteService.getTeamsList = function() 
    {
        var list = $resource(remotePaths.root+remotePaths.teams.index).query(function() {
            $log.log(list);
          });
        return list;
	}
    remoteService.createTeam = function(name, info, leaderID, assistantID) 
    {
        var team = $resource(remotePaths.root+remotePaths.teams.create,{});
        var params = {'name': name,
                      'info': info,
                      'leader_id': leaderID,
                      'assistant_id': assistantID };
        var result = team.get(params, function() {
            $log.log('Creation team responce:' + result);
        });
        return result[0];
	}

    // News ----------------------------------------------------------------------
    remoteService.getNewsList = function() 
    {
        var defer = $q.defer();
        var news =$resource(remotePaths.root+remotePaths.news);
        
        news.query(function(result) {
            $log.log('News List:' + result);
            defer.resolve(result);
          });
        return defer.promise;
	}

    // inftastructure ------------------------------------------------------------
    remoteService.inftastructure = {
        'getRepositories' : 
            function() 
            {
                var defer = $q.defer();
                var news =$resource(remotePaths.root+remotePaths.infrastructure.repositories);
                
                news.query(function(result) {
                    $log.log('Repositories List:' + result);
                    defer.resolve(result);
                });
                return defer.promise;
            }  
    }  
  }