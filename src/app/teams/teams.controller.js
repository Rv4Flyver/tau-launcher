(function() {
  'use strict';
  angular
    .module('tauLauncher')
    .controller('TeamsController', TeamsController);

  /** @ngInject */
  function TeamsController($log) 
  {
    var vm = this;
    
    vm.paging = {
      'total' : 0,
      'current'  :  1,
      'limit'  :  10,
      'start'  :  0
    }
    
    
    vm.setPage = function (pageNo) {
      vm.paging.current = pageNo;
    };
    
    vm.pageChanged = function () {
       $log.log('Page changed to: ' + vm.paging.current);
       vm.paging.start = (vm.paging.current - 1) * vm.paging.limit;
    };
    
    
  }
})();
