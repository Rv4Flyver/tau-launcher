'use strict';

angular.module('tauLauncher')
    .controller('OptionsController', OptionsController);

  /** @ngInject */
  function OptionsController(ModalsService) 
  {
    var vm = this;

    vm.openCfgEditor = function() {
        ModalsService.openCfgEditor();
    }
    
  }
