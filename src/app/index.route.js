(function() {
  'use strict';

  angular
    .module('tauLauncher')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    
    // R O U T I N G
    $stateProvider
      .state('base', {
        url: '',
        templateUrl: 'app/base/base.view.html',
        controller: 'BaseController',
        controllerAs: 'base'
      })
        .state('base.main', {
          url: '/main',
          templateUrl: 'app/main/main.view.html',
          controller: 'MainController',
          controllerAs: 'main'
        })
        .state('base.games', {
          url: '/games',
          templateUrl: 'app/games/games.view.html',
          controller: 'GamesController',
          controllerAs: 'games'
        })
        // CUSTOM GAMES STATES
          .state('base.games.custom', {
            url: '/custom',
            templateUrl: 'app/games.custom/custom.view.html',
            controller: 'CustomGamesController',
            controllerAs: 'customGames'
          })
            .state('base.games.custom.create', {
              url: '/create',
              templateUrl: 'app/games.custom.create/create.view.html',
              controller: 'CreateCustomGameController',
              controllerAs: 'createCustomGame'
            })
            .state('base.games.custom.join', {
              url: '/join',
              templateUrl: 'app/games.custom.join/join.view.html',
              controller: 'JoinCustomGameController',
              controllerAs: 'joinCustomGame'
            })
        .state('base.news', {
          url: '/news',
          templateUrl: 'app/news/news.view.html',
          controller: 'NewsController',
          controllerAs: 'news'
        })
        .state('base.players', {
          url: '/players',
          templateUrl: 'app/players/players.view.html',
          controller: 'PlayersController',
          controllerAs: 'players'
        })
        .state('base.teams', {
          url: '/teams',
          templateUrl: 'app/teams/teams.view.html',
          controller: 'TeamsController',
          controllerAs: 'teamsCtrl'
        })
          .state('base.teams.create', {
            url: '/create',
            templateUrl: 'app/teams.create/create.view.html',
            controller: 'CreateTeamController',
            controllerAs: 'createCtrl'
          })
        .state('base.tv', {
          url: '/tv',
          templateUrl: 'app/tv/tv.view.html',
          controller: 'TVController',
          controllerAs: 'tv'
        })
        .state('base.options', {
          url: '/options',
          templateUrl: 'app/options/options.view.html',
          controller: 'OptionsController',
          controllerAs: 'options'
        })
        .state('base.statistics', {
          url: '/statistics',
          templateUrl: 'app/statistics/statistics.view.html',
          controller: 'StatisticsController',
          controllerAs: 'statistics'
        })
        ;

    $urlRouterProvider.otherwise('/main');

  }

})();
