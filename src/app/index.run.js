(function() {
  'use strict';

  angular
    .module('tauLauncher')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
