(function() {
  'use strict';

  angular
    .module('tauLauncher')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $provide) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    // D E F I N E  C O N S T A N T S
    var path = require('path');
    var nwPath = process.execPath;
    $provide.constant('rootDir', path.dirname(nwPath));
  }

})();
