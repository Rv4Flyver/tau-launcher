  'use strict';

  angular
      .module('tauLauncher')
      .service('messagingService', messagingService);

/** @ngInject */
function messagingService(UtilsService, defaultResources, $log, $rootScope, $q ,IRC) 
{
    var vm = this;

    var chatClient = null;
    var usersOnlineList = [];

    
    vm.startClient = function(login) {
        var defer = $q.defer();

        var options = {
            autoConnect: false,
            channels: [IRC.channel],
            debug: true,
            showErrors: true,
            stripColors: true,
            encoding: "UTF-8",
            retryDelay: 10000
        };

        chatClient = UtilsService.irc.connect(IRC.server, IRC.users_prefix + login, options);
        AddChatListeners();
        chatClient.connect(0, function(response){
            $log.log('irc: (connect): ', response);
            defer.resolve(response);
        });
        return defer.promise;
    }
    
    /**************************************************
        Events Handling
    ***************************************************/
    function AddChatListeners() {
        // ERRORS handling
        chatClient.addListener('error', function(message) {
            $log.error('irc: (error): ', message);
        });

        // NAMES get names list from channels
        chatClient.addListener('names', function(channel, nicksObject) {
            $log.log('irc: (names): ' + channel + '[');
            for (var property in nicksObject) {
                var nickName = property.replace(IRC.users_prefix,'');
                if (nicksObject.hasOwnProperty(property)) {
                    $log.log(nicksObject[property] + property);
                    
                    if (!usersOnlineList.some(function(e) {e.name == nickName}) && property.indexOf(IRC.users_prefix) == 0) { // if not exists in list && prefixed with hlag_
                        usersOnlineList.push({
                            "name"      : nickName,
                            "status"    : "online",
                            "role"      : nicksObject[property]
                        });
                    }
                }
            }
            $log.log(']');

            $rootScope.$broadcast('IRC_NicknamesChanged', usersOnlineList);
        });

        // MESSAGES
        chatClient.addListener('message', function(nick, to, text, message) {
            $log.log('irc: (message): ', nick, to, text, message);
        });

        // RAW Messages from the server
        chatClient.addListener('raw', function(message) {
            $log.log('irc: (raw): ', message.rawCommand);
        });

        // CHANGE NICK from the server
        chatClient.addListener('nick',  function (oldnick, newnick, channels, message) { 
            var oldNickPrefixless = oldnick.replace(IRC.users_prefix,'');
            var newNickPrefixless = newnick.replace(IRC.users_prefix,'');
            $log.log('irc: (nick): ', oldnick, newnick, channels, message);
            for(var i = 0; i < usersOnlineList.length; i++ ) {
                if(usersOnlineList[i].name == oldNickPrefixless)
                {
                    usersOnlineList[i].name = newNickPrefixless;
                }
            }
            $rootScope.$broadcast('IRC_NicknamesChanged', usersOnlineList);
        });
        
    }
    /**************************************************
        Client Functions
    ***************************************************/
    vm.auth = function(login,chatPasswd) {
                chatClient.send('auth', login, chatPasswd);
    };

    // Joins the specified channel.
    // Arguments:	
    //     channel (string) – Channel to join
    //     callback (function) – Callback to automatically subscribed to the join#channel event, 
    //                             but removed after the first invocation. channel supports multiple JOIN arguments 
    //                             as a space separated string (similar to the IRC protocol).
    vm.join =  function(channel,callback) {
        chatClient.join(channel, callback)
    };

    // Sends a message to the specified target.
    // Arguments:	
    //     target (string) – is either a nickname, or a channel.
    //     message (string) – the message to send to the target.
    vm.say = function(target, message) {
        chatClient.say(target, message)
    };

    vm.sendCmd = function(commandsAsArray) {
        chatClient.send.apply(this, args);
    }
}