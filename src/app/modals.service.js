'use strict';

angular
    .module('tauLauncher')
    .service('ModalsService', ModalsService);

/** @ngInject */
function ModalsService($uibModal) 
{
    var vm = this;
    
    /*******************************************************************
     * MODALS
     *        DEFINITIONs
     *******************************************************************/
    vm.login = function(){
        $uibModal.open({
            animation: true,
            templateUrl: 'app/components/modals/login.html',
            controller: LoginModalCtrl,
            size: 'sm',
            resolve: {

            }
        });
    };
    vm.registerUser = function(){
        $uibModal.open({
            animation: true,
            templateUrl: 'app/components/modals/signup.user.html',
            controller: RegisterUserModalCtrl,
            size: 'sm',
            resolve: {

            }
        });
    };
    vm.message = function(msg){
        $uibModal.open({
            animation: true,
            templateUrl: 'app/components/modals/message.html',
            controller: function($scope, $uibModalInstance){
                $scope.msg = msg;
                $scope.dismiss = function(){
                    $uibModalInstance.dismiss('cancel');
                }
            },
            size: 'sm',
            resolve: {

            }
        });
    };
    vm.openCfgEditor = function(){
        $uibModal.open({
            animation: true,
            templateUrl: 'app/components/modals/cfgEditor.html',
            controller: function($scope,$uibModalInstance,gameService){
                //var vm = this;
                $scope.model = {
                    configData  : gameService.config.get(),
                    selected    : -1,
                    searchText  : ""
                };

                $scope.editConfig = function (idx) {
                    $scope.model.selected = idx;
                };

                $scope.saveConfig = function () {
                    gameService.config.save($scope.model.configData);
                };

                $scope.reset = function () {
                    $scope.model.selected = -1;
                };

                $scope.dismiss = function(){
                    $uibModalInstance.dismiss('cancel');
                }
            },
            size: 'fullwidth',
            resolve: {

            }
        });
    };
   
    /********************************************************************
     * CONTROLLERS
     *              DEFINITIONs
     ********************************************************************/
    function LoginModalCtrl($scope, UserService,$uibModalInstance, $location)
    {
        console.log('LoginModalCtrl');
        
        $scope.email = '';
        $scope.password = '';
        
        $scope.unlockErrors = false;
        
        $scope.ForgotPassword = function() {
            console.log('ForgotPassword Request');
        };
        
        $scope.Login = function() {
            if($scope.loginForm.$valid)
            {
                var userCredentials = {
                    email:    $scope.email,
                    password: $scope.password
                };
                
                $scope.showLoaderGIF = true;
                
                UserService.login(userCredentials).then(
                    function(response) 
                    {
                        $uibModalInstance.dismiss('cancel');
                    },
                    function(response)
                    {
                        $scope.showLoaderGIF = false;
                        
                        var errorString = "";
                        if(!!response.data){
                            // Get Standart Errors Messages
                            if(!!response.data.message){
                                $scope.responseError = response.data.message;
                            } else {
                                $scope.responseError = 'An error has occured.'
                            }
                        } 
                        else { // Server returned nothing  or Other Error Type
                            $scope.showLoaderGIF=false;
                            if(!!response.response) {
                                $scope.responseError = response.response;
                            } 
                            else {
                                $scope.responseError = "Unable to receive data from server";
                            }
                        }
                    }
                );
            }
            else { // show errors on the form
                $scope.unlockErrors = true;
            }
        };
        
        $scope.loginFacebook = function() {
            //window.location = apiEndpoints.loginFacebook; //todo read from constants
            $uibModalInstance.dismiss('cancel');
        }
    }
   
    function RegisterUserModalCtrl($scope, UserService, ModalsService, $uibModalInstance) 
    {
        console.log('RegisterUserModalCtrl');
        
        $scope.login = '';
        $scope.sex = '';
        $scope.country = '';
        $scope.city = '';
        $scope.email = '';
        $scope.password = '';
        $scope.passwordCheck = '';
        $scope.team = 0;
        
        $scope.remainSigned = false;
        
        $scope.unlockErrors = false;
        
        $scope.responseError = null;
        $scope.responseSuccessMessage = null;
        
        $scope.isSuccessfullSignup = false;
        
        // $scope.validFormData = ( $scope.form.sal.$valid && $scope.form.firstName.$valid 
        //                        && $scope.form.lastName.$valid && $scope.form.mobile.$valid 
        //                        && $scope.form.zip.$valid && $scope.form.city.$valid 
        //                        && $scope.form.adres.$valid && $scope.form.email.$valid
        //                        && $scope.form.password.$valid && $scope.form.passwordCheck.$valid);
                
        $scope.Signup = function() {
            if($scope.signupForm.$valid) 
            {
                var userData = {
                    login :         $scope.login,
                    sex:            $scope.sex,
                    country :       $scope.country,
                    city :          $scope.city,
                    email :         $scope.email,
                    password :      $scope.password,
                    passwordCheck : $scope.passwordCheck,
                    team:           $scope.team,
                    role:           'player'
                };

                $scope.showLoaderGIF=true;
                
                UserService.register(userData).then(
                    function(result) {
                        if(result.code == 0) {
                            $scope.isSuccessfullSignup = true;
                            $scope.responseSuccessMessage = "Registered";
                        } else {
                            $scope.isSuccessfullSignup = false;
                            $scope.responseFailureMessage = result.message;
                        }
                        
                        $scope.showLoaderGIF = false; // Hide Loading Icon
                        UserService.authorize();
                    }
                );

            }
            else { // show errors on the form
                $scope.unlockErrors = true;
            }
        };
        
        $scope.signupFacebook = function() {
            window.location = apiEndpoints.signUpCustomerFacebook; //todo read from constants
            $uibModalInstance.dismiss('cancel');
        };
        
        $scope.Dismiss = function(){
            $uibModalInstance.dismiss('cancel');
        }
    }

}