  'use strict';

  angular
      .module('tauLauncher')
      .service('mapsService', mapsService);

/** @ngInject */
function mapsService(gameService, gameDir, localURLs, UtilsService, defaultResources, $log) 
{
    var vm = this;
    var mapPreviewsPath = UtilsService.joinPath(gameService.getGameDir(), localURLs.maps.previews);


    vm.getLocalMapsList = function() 
    {
        var fs = require('fs');
        var localMapsFullPath = UtilsService.joinPath(gameService.getGameDir(), localURLs.maps.dir);
        var maps = fs.readdirSync(localMapsFullPath);

        var mapsList = EnrichMapsList(maps);

        angular.forEach(mapsList, function(map) {
            map.rank = Math.random();
        });
        return mapsList;
    }

    vm.getMapsList = function() {
        var mapsJSONPath = UtilsService.joinPath(gameService.getGameDir(), localURLs.maps.json);

        var mapsList = [];
        var packages = UtilsService.readJSONFile(mapsJSONPath);
        
        for (var i in packages) 
        {
            $log.log("| package " + packages[i].package);
            mapsList = mapsList.concat(packages[i].maps);
        }

        mapsList = EnrichMapsList(mapsList);

        return mapsList;
    }

    function EnrichMapsList(maps) {
        var mapsOBJs = [];
        for (var i in maps) 
        {
            var title = maps[i].substr(0, maps[i].lastIndexOf('.'));
            var extension = maps[i].substr(maps[i].lastIndexOf('.') + 1);
            if(extension == "bsp" || extension == "map") // Hack for case when all bsp packed // TO DO implement read of .pk3 archives
            {
                // Search for map preview or use default
                var preview = defaultResources.map;
                //alert(preview);
                var fs = require('fs');
                try {
                    // Query the entry
                    var stats = fs.lstatSync(UtilsService.resolvePath(mapPreviewsPath, title + ".jpg"));
                    // Is it a directory?
                    // Check is redundant fs.lstatSync raise exception if file or path does not exists
                    if (stats.isFile()) { 
                        preview = "file:///../" + gameDir + localURLs.maps.previews + title + ".jpg";
                    }
                }
                catch (e) {
                    $log.log("| use default map preview");
                }
                //var regExp = new RegExp('[.]' + extension + '$'); // Hack for case when all bsp packed // TO DO implement read of .pk3 archives
                // Add map to maps array
                mapsOBJs.push({
                  'title': title,
                  'name': maps[i],                            // .replace(regExp,".bsp"), // Hack for case when all bsp packed // TO DO implement read of .pk3 archives
                  'url': 'http://maps.headdrop.ru/'+maps[i],  // .replace(regExp,".bsp"), // Hack for case when all bsp packed // TO DO implement read of .pk3 archives
                  'description': 'Description of ' + title + '...',
                  'logo': defaultResources.logo,
                  'preview': preview
                });          
            }
        }
        angular.forEach(mapsOBJs, function(map) {
            map.rank = Math.random();
        });
        return mapsOBJs;
    }
}