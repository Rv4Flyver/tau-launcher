'use strict';

angular
	.module('tauLauncher')
	.service('updaterService', updaterService);

/** @ngInject */
function updaterService($rootScope, UtilsService, gameService,launcherService,remoteService, $log) 
{
	var updaterService = this;

	
	updaterService.isWorking = false;
	updaterService.downloadProgress = 0;
	
	updaterService.updateGameContent = function(updateSuccessfullCallback,noUpdateNeededCallback) {
        var localChecksumsFile = UtilsService.resolvePath(gameService.getGameDir(), "checksums.json");
        remoteService.inftastructure.getRepositories().then(function(repositories) {
                UpdateRequest(
                    {
                        protocol :  repositories[0].protocol + ':',
                        host:       repositories[0].host,
                        port:       repositories[0].port,
                        path: '/' + repositories[0].path + 'checksums.json',
                        method: 'GET'
                    },
                    repositories[0].protocol + "://" + repositories[0].host + ':' + repositories[0].port + "/" + repositories[0].path + "hlag-nightly.zip",
                    gameService.getGameDir(),
                    localChecksumsFile,
                    updateSuccessfullCallback,
                    noUpdateNeededCallback
                );
            }
        );
    };

	updaterService.updateLauncher = function(updateSuccessfullCallback,noUpdateNeededCallback) {
        var localChecksumsFile = UtilsService.resolvePath(launcherService.getLauncherDistDir(), "checksums-launcher.json");
        remoteService.inftastructure.getRepositories().then(function(repositories) {
                UpdateRequest(
                    {
                        protocol :  repositories[0].protocol + ':',
                        host:       repositories[0].host,
                        port:       repositories[0].port,
                        path: '/' + repositories[0].path + 'checksums-launcher.json',
                        method: 'GET'
                    },
                    repositories[0].protocol + "://" + repositories[0].host + ':' + repositories[0].port + "/" + repositories[0].path + "dist.zip",
                    launcherService.getLauncherDistDir(),
                    localChecksumsFile,
                    function() {
                        updateSuccessfullCallback();
                        launcherService.Restart();    
                    },
                    noUpdateNeededCallback
                );
            }
        );
    };

	/****************
	* C O M M O N 
	*****************/
    function UpdateRequest(options, downloadURL, targetDir, localChecksumsPath, successCallback, noUpdateNeededCallback) 
	{
		/**********
		* Get Files 
		***********/

		// getting newest checksums:      
        var serverChecksums = '';
		var request = UtilsService.net.http.request(options, function (res) {
			res.on('data', function (chunk) {
				$log.log(chunk);
				serverChecksums += chunk;
			});
			res.on('end', function () { //md5
				$log.log("Checksums downloaded: " + serverChecksums);
				
				// getting client checksums:
				var clientChecksums = ".";
				if(UtilsService.isFileExists(localChecksumsPath))
				{
					clientChecksums = UtilsService.readFile(localChecksumsPath);
				}
                else {
                    $log.log("Local checksusms file does not exists.");
                }
                // compareing client's and server's checksums and if not muched
				// update clent's content:
                $log.log("Check if server checksums matches local checksums");
				if (serverChecksums != clientChecksums) 
				{
                    updaterService.isWorking = true; // start working
					/*******************
					* Remove old content 
					********************/
					RecreateFolder(targetDir);
					
					/*****************
					* Download content
					******************/
					Download(downloadURL, UtilsService.resolvePath(targetDir, "content.zip"), targetDir, 
                            function() {
                                /*****************
                                * Update Checksums
                                ******************/
                                UtilsService.write2File(localChecksumsPath, serverChecksums);   // TO DO checksums should be included into archive
                                successCallback();
                            }
                    );	
				} 
                else 
                {
                    noUpdateNeededCallback();
                }
			});
		});
		request.on('error', function (e) {
			$log.log(e.message);
			// reset state variabless
			ResetStateVariables() 
		});
		request.end();
	
		/****************
		* Check checksums 
		*****************/
		// server.md5: ["pack01.pk4": "18e904aae79b5642ed7975c0a0074936", "pack02.pk4": "18e904aae79b5642ed7975c0a0074936" ]
		// foreach key-value do /base/{key}==element.key - ok ELSE false(reload)
		// var md5File = require('md5-file')
		// md5File('./path/to/a_file') // '18e904aae79b5642ed7975c0a0074936' 
		// md5File('./path/to/a_file', function (error, sum) {
		//   if (error) return $log.log(error)
		//   $log.log(sum) // '18e904aae79b5642ed7975c0a0074936' 
		// })

	}

	function RecreateFolder(path) 
	{
        //UtilsService.deleteFolderRecursive(path);
        UtilsService.createFolder(path);
	}
	
	
	function Download(sourceURL, archivePath, extractDir, successCallback)
	{
		var file = UtilsService.createWriteStream(archivePath);
		
		var downloadRequest = UtilsService.net.http.get(sourceURL, function(response) {
			var len = parseInt(response.headers['content-length'], 10);
			var downloaded = 0;
			response.pipe(file);
			response.on('data', function(chunk) {
							downloaded += chunk.length;
							UpdateProgress((100.0 * downloaded / len).toFixed(2))
							//$log.log("Downloading " + (100.0 * downloaded / len).toFixed(2) + "% " + downloaded + " bytes");
						}).on('end', function () {
							$log.log(sourceURL + ' downloaded to: ' + archivePath);
							updaterService.downloadProgress = 0;
						}).on('error', function (err) {
							$log.log("Error occured while downloading " + sourceURL);
                            $log.error(err);
							ResetStateVariables();
						});  
			file.on('finish', function() {
				file.end();
				$log.log("Downloaded file.");
				
				/*****************
				* Extract archive 
				******************/
				ExtractArchive(archivePath,extractDir, successCallback);
			});
		});
		downloadRequest.on('error', function(err) {
			$log.log("Error occured while download request: ");
            $log.error(err);
			ResetStateVariables();
		});
	}
	
	function ExtractArchive(path, targetDir, successCallback)
	{
		$log.log("Begin extraction.");
		UtilsService.zip.extractAllTo(path, targetDir);  
		$log.log("Extraction completed.");

		$log.log("Delete " + path );
		UtilsService.deleteFile(path);
		ResetStateVariables();
        successCallback();
	}
	
	function ResetStateVariables() 
	{
			// reset state variabless
			updaterService.isWorking = false; // stop working
			UpdateProgress(0);
	}
	
	function UpdateProgress(value)
	{
		updaterService.downloadProgress = value;
		$rootScope.$broadcast('UpdateProgressChanged', 'changed');
	}
}