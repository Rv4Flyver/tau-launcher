'use strict';

angular.module('tauLauncher')
  .controller('CreateCustomGameController', CreateCustomGameController);
  
  function CreateCustomGameController($state, gameService, mapsService, $log) {
    var vm = this;
    
    vm.maps = mapsService.getLocalMapsList().concat( mapsService.getMapsList() );
    vm.server = {
      "isDedicated": true,
      "ipAddress": "127.0.0.1",
      "playersNumb": 10,
      "gameType": "deathmatch"
    };
    vm.searchMapText = '';

    vm.startMap = function(map){
      gameService.startMap(map);
    }
    
    vm.Create = function(server){
      if(!server.ipAddress.length && confirm("You do not specified an ip address, should localhost be used instead?")) 
      {
        server.ipAddress = "127.0.0.1";
      }
      if(!server.ipAddress.length) 
      {
        alert("Ip is not specified!");
        return;
      }
      if(!server.map ) 
      {
        alert("Please select the map.");
        return;
      }
      $log.log(server);
      gameService.createCustomServer(server);
    }
  }
