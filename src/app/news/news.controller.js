(function() {
  'use strict';
  angular
    .module('tauLauncher')
    .controller('NewsController', NewsController);

  /** @ngInject */
  function NewsController(remoteService) 
  {
    var vm = this;
    
    vm.newsList = [];

    remoteService.getNewsList().then(function(news){
      vm.newsList = news;
    });

  }
})();
