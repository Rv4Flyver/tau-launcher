  'use strict';

  angular
      .module('tauLauncher')
      .service('gameService', gameService);

  /** @ngInject */
  function gameService(rootDir, gameDir, UtilsService, $log) 
  {
    var gameService = this;
    var exec = require('child_process').exec;
    
    gameService.getGameDir = function()
    {
      return UtilsService.resolvePath(rootDir, gameDir);
    }
    
    // Run Game Related Methods:
    var headDropPath = UtilsService.resolvePath(gameService.getGameDir(), "fteqw64.exe -game hlag -nohome");
    var headDropServerPath = UtilsService.resolvePath(gameService.getGameDir(), "fteqw64.exe -game hlag -nohome -dedicated");
    var options = {
      "cwd" : gameService.getGameDir()
    };
    
    // Start Game
    gameService.startGame = function()
    {
      var params = ' ';
      runExecutable(headDropPath, params); 
      $log.log("| Execute: " + headDropPath);
    }
    
    // Start Map
    gameService.startMap = function(map) 
    {
      var params = ' +map ' + map;
      runExecutable(headDropPath, params);
      $log.log("| Execute: " + headDropPath + params);
    }
    
    //Start Custom Server
    gameService.createCustomServer = function(server)
    {
      var params = ' ';    
      // is Pure
      if(!!server.isPure && server.isPure)
      {
        params += ' +si_pure 1 ';
      }
      else
      {
        params += ' +si_pure 0 ';
      }      
      
      // game type: deathmatch...
      if(!!server.gameType)
      {
        params += " +si_gametype " + server.gameType + ' '; 
      } 
      else
      {
        params += ' +si_gametype "deathmatch" '; 
      } 
      // do servermaprestart if server is not dedicated
      if(!server.isDedicated) 
      {
        params += ' +servermaprestart '; 
      }
      // map
      if(!!server.map)
      {
        params += ' +si_map ' + server.map.name + ' ';
      }
      else
      {
        params += ' +si_map t_one ';
      }
      
      // is server dedicated
      if(server.isDedicated) 
      {
        params += " +spawnServer ";
        runExecutable(headDropServerPath, params);       
      }
      else
      {
        runExecutable(headDropPath, params);
      }     
    } //Start Custom Server
    
    //Join Custom Server
    gameService.joinCustomServer = function(ip,port)
    {   
       var params = " +connect " + ip;       //TODO add port value to params
       runExecutable(headDropPath, params);
    } 
    
    /***********************
    * Get config as JSON to 
    * make it easy to edit 
    ************************/    
    gameService.config = {
      getPath : function()
      {
          var modPath =  UtilsService.resolvePath(gameService.getGameDir(), 'hlag'); // TODO Refactor
          return UtilsService.resolvePath(modPath, 'fte.cfg');
      },
      get : function() 
      {
        var config = [];
        var text = UtilsService.readFile(gameService.config.getPath(),'utf8');
        var textLines = text.split('\n');
        
        var pattern = new RegExp(/^([-+_A-Za-z0-9]+)[ ]?([A-Za-z~`!@#$%^&*()_*+-="'№;:?|{}\[\]<>\\]+)?[ ]?(".+")?[ ]*(\/\/.+$)?/);
        var oneLineCommentatyPattern = new RegExp(/^\s*\/\/.+$/);
        var replaceExp = new RegExp('"', 'g');
        var whitespacesExp = new RegExp('  ', 'g');
        for(var n = 0; n < textLines.length; n++) 
        {
          var line = {
            "id" : n
          }
          var processedLine = textLines[n].replace(whitespacesExp,' ');
          // One Line Commentary Check
          if(oneLineCommentatyPattern.test(processedLine)) 
          {
              line.commentary = processedLine;
              config.push(line);
              continue;
          }
          // Try to parse line
          var values = processedLine.match(pattern);
          if(values) {
            line.type           = values[1].replace(replaceExp,'');
            line.key            = values[2] ? values[2].replace(replaceExp,'') : null;
            line.value          = values[3] ? values[3].replace(replaceExp,'') : null;
            line.commentary     = values[4] ? values[4] : null;
          }

          config.push(line);
          //if(config.length == 10) break; // TODO Test
        }
        return config;
      },
      /**********************
      * Recreate config from 
      * the config object
      ***********************/
      save : function(config) 
      {
        var text = "";
        for(var n = 0; n < config.length; n++)
        {
          text += config[n].type;
          text += config[n].key == undefined ? "" : " " + config[n].key;
          text += config[n].value == undefined ? "" : " " + config[n].value;
          text += config[n].commentary == undefined ? "" : " " + config[n].commentary;
          text += "\n";
        } 
        UtilsService.write2File(gameService.config.getPath(), text);
        
      }
    }
    
    
    function runExecutable(fullPath, params)
    {
      params += " -nohome"; // TODO Refactor
      $log.log("| Execute: " + fullPath + params);       
      
      var game = exec(fullPath + params,  options,
        function (error, stdout, stderr) {
          $log.log('stdout: ' + stdout);
          $log.log('stderr: ' + stderr);
          if (error !== null) {
            $log.log('exec error: ' + error);
          }
      });

      return game;
    }

  }