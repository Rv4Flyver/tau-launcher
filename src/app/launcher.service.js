'use strict';

  angular
      .module('tauLauncher')
      .service('launcherService', launcherService);

  /** @ngInject */
  function launcherService(UtilsService, rootDir,$log) 
  {
    var launcherService = this;
    var gui = require('nw.gui');    // Load native UI library
    var win = gui.Window.get();     // Get the current window

    launcherService.getLauncherDistDir = function()
    {
      var tauDir = UtilsService.resolvePath(rootDir, 'tau');  
      return UtilsService.resolvePath(tauDir, 'dist');
    }
    // _______________________________________________
    // Common functions of interaction with the window
    // Close
    launcherService.Close = function()
    {
        win.close();
    }
    // Minimize
    launcherService.Minimize = function()
    {
        win.minimize();
    }
    // Maximize
    launcherService.Maximize = function()
    {
        win.maximize();
    }
    // Unmaximize
    launcherService.Unmaximize = function()
    {
        win.unmaximize();
    }   
    // Restore
    launcherService.Restore = function()
    {
        win.restore();
    }     
    // // Open
    // launcherService.OpenWindow = function()
    // {
    //     win.open("/", {}, function() {
    //         $log.log("New window opened!");
    //     });
    // }     
    // _________________________________________________
    // Extended functions of interaction with the window    
    // - ToggleKioskMode
    launcherService.ToggleKioskMode = function()
    {
        win.toggleKioskMode();
    }  
    // - Restart
    launcherService.Restart = function()
    {
        chrome.runtime.reload();
    }  
    
    // _________________________________________________    
  }