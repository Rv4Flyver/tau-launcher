(function() {
  'use strict';
  angular
    .module('tauLauncher')
    .controller('CreateTeamController', CreateTeamController);

  /** @ngInject */
  function CreateTeamController($timeout, gameService, mapsService, remoteService) 
  {
    var ctrl = this;
    
    ctrl.name = '';
    ctrl.info = '';
    ctrl.leaderID = 1;
    ctrl.assistantID = 1;
    
    ctrl.Create = function() 
    {
      remoteService.createTeam(ctrl.name, ctrl.info, ctrl.leaderID, ctrl.assistantID);
    }
  }
})();
