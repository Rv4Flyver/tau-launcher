/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('tauLauncher')
      .constant('debug', true)
    // PATH RELATED
      //.constant('rootDir', '')      // defined at config stages
      .constant('gameDir', 'game')
      .constant('dataDir','data')
      // Dir URLs
      .constant('mapsURL', '/hlag/maps/')
      .constant('mapsPreviewsURL', '/hlag/levelshots/')
      .constant('localURLs', {
          'maps'        : {
              'dir'         : '/hlag/maps/',
              'previews'    : '/hlag/levelshots/',
              'json'        : '/id1/maps.json'
          }
      })
    // DEFAULTS RESOURCES
      .constant('defaultResources', {
          'map':  '../../assets/images/maps/default.png',
          'logo': '../../assets/images/adrenaline-tournament-logo.png'
        })
    // CONSOLE PARAMS
      .constant('consoleParams', {
          'gametype':  'si_gametype',
          'map': 'si_map',
          'dedicated': 'net_serverDedicated'
        })
    // SERVICE PATHs
      .constant('remotePaths', {
          'root':  'https://hlag-rvach.rhcloud.com/index.php/',
          //'root':  'http://localhost/index.php/',
          'players': {
            'index' : 'players',
            'create': 'players/create',
            'update': 'players/update'
          },
          'teams': {
            'index' : 'teams',
            'create': 'teams/create',
            'update': 'teams/update'
          },
          'tournaments'   : 'games/tournaments',
          'championships' : 'games/championships',
          'customGames'   : 'games/custom',
          'games': {
              'custom'    : 'games/custom'
          },
          'servers' : {
            'index'   : 'servers',
            'arenas'  : {
                'index' : 'servers/arenas',
                'all'   : 'servers/arenas/all',
                'free'  : 'servers/arenas/free'
            }
          },
          'news'          : 'news',
          'infrastructure': {
            'repositories'  : 'infrastructure/repositories'
          }
        })
    // MESSAGING
        .constant('IRC', {
            'server':       'irc.quakenet.org',
            'port':         6667,
            'channel':      '#hlag',
            'users_prefix': 'hlag_' // used to distinguish HLAG clients among others
            })
})();
