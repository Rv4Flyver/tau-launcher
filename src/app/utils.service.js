'use strict';

angular
    .module('tauLauncher')
    .service('UtilsService', UtilsService);

/** @ngInject */
function UtilsService($log,$interval) 
{
    var _path = require('path');
    var _fs = require('fs');
    var _httpsecure = require('https');
    var _http = require('http');
    var _AdmZip = require('adm-zip');
    var _irc = require('irc');
    var _net = require('net');
    var _dns = require('dns')
    var dgram = require('dgram');
    var vm = this;
    
    vm.normalizePath = function(path) 
    {
        return _path.normalize(path);
    }

    vm.resolvePath = function(a, b) 
    {
        a = _path.normalize(a);
        b = _path.normalize(b);
        return _path.resolve(a, b);
    }

    vm.joinPath = function(a, b) 
    {
        a = _path.normalize(a);
        b = _path.normalize(b);
        return _path.join(a, b);
    }

    /**************************************************************************
        Server Communication Functions
    ***************************************************************************/
    
    // Files #################################################################
    vm.copyFile = function(source, target, cb) {
        var cbCalled = false;

        var rd = _fs.createReadStream(source);
        rd.on("error", function(err) {
            done(err);
        });
        var wr = _fs.createWriteStream(target);
        wr.on("error", function(err) {
            done(err);
        });
        wr.on("close", function(ex) {
            done();
        });
        rd.pipe(wr);

        function done(err) {
            if (!cbCalled) {
                cb(err);
                cbCalled = true;
            }
        }
    }

    vm.isFileExists = function(filePath) {
        return _fs.existsSync(filePath)
    }

    vm.readFile = function(filePath) {
        return _fs.readFileSync(filePath,'utf8')
    }

    vm.write2File = function(filePath, data){
        _fs.writeFileSync(filePath, data);
    }

    vm.createWriteStream = function(filePath){
        return _fs.createWriteStream(filePath);
    }

    vm.deleteFile = function(filePath){
        _fs.unlink(filePath); 		// fs.unlinkSync(filePath);
    }

    vm.readJSONFile = function(jsonFile) {
        var localData = null;
        try{
            localData = _fs.existsSync(jsonFile) ? angular.fromJson(_fs.readFileSync(jsonFile,'utf8')) : {}
        }
        catch(e) {
            $log.log(e);
        }
        return localData;
    }

    // Folders #################################################################
    vm.removeFolder = function(path){
		var rmdir = require('rimraf');
		rmdir.sync(path);
    }
    vm.deleteFolderRecursive = function(path) {
        if( path.indexOf('node_modules') == -1 && _fs.existsSync(path) ) {
            _fs.readdirSync(path).forEach(function(file,index){
                var curPath = _path.join(path, file);
                if(_fs.lstatSync(curPath).isDirectory()) { // recurse
                    vm.deleteFolderRecursive(curPath);
                } else { // delete file
                    _fs.unlinkSync(curPath);
                }
            });
            _fs.rmdirSync(path);
        }
    };

    vm.createFolder = function(path){

		// $ npm install mkdirp
		var mkdirp = require('mkdirp');
		mkdirp(path, function(err) { 
			$log.log("Cannot create directory " + path);
            $log.error(err);
		});
    }

    /**************************************************************************
        Server Communication Functions
    ***************************************************************************/
    vm.net = {
        resolveHostname : 
            function(hostname, cb) {
                _dns.lookup(hostname, cb);
            },
        testTCPPort :       
            function(port, host, cb) {
                _net.createConnection(port, host).on("connect", function(e) { 
                    cb("success", e); 
                }).on("error", function(e) {
                    cb("failure", e);
                });
            },
        testUDPPort :       
            function(port, host, cb) {
                var buffer = new Buffer([0x80, 0x00, 0x00, 0x0c, 0x02, 0x51, 0x55, 0x41, 0x4b, 0x45, 0x00, 0x03]);
                var client = dgram.createSocket('udp4');
                var timeoutFn;
                client.on( "message", function( msg, rinfo ) {
                    $log.log( "The packet came back: " + msg );
                    cb("success", msg);
                    $interval.cancel(timeoutFn);
                    client.close();
                });
                client.send(buffer, 0, buffer.length, port, host, function(err, bytes) {
                    if (err) {
                        cb("failure", err);
                        $log.log('UDP was not sent:'+ err);
                    } else {
                        $log.log('UDP message sent to ' + host +':'+ port);
                        timeoutFn = $interval(function(){
                                cb("failure", "timeout");
                                client.close();
                        },5000,1);
                    }
                });
            },
        https : {
            request : function(options, func) {
                return _httpsecure.request(options, func);
            },
            get     : function(url, func) {
                return _httpsecure.get(url, func);
            }
        },
        http : {
            request : function(options, func) {
                return _http.request(options, func);
            },
            get     : function(url, func) {
                return _http.get(url, func);
            }
        }
    }

    /**************************************************************************
        Zip Functions
    ***************************************************************************/
    vm.zip = {
        extractAllTo : function(archivePath,targetDir) {
            var zip = new _AdmZip(archivePath);
            zip.extractAllTo(targetDir, /*overwrite*/true);  
        }
    }

    /**************************************************************************
        Instant Chat
    ***************************************************************************/
    vm.irc = {
        chatClient : null,
        connect : function(server, login, optionsObj) {
            /* 
                _irc.Client is the base of everything, it represents a single nick connected to a single IRC server.
            */
            return new _irc.Client(server, login, optionsObj);
        }
    }
}