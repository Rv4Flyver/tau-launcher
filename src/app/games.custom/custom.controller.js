'use strict';

angular.module('tauLauncher')
  .controller('CustomGamesController', CustomGamesController);
  
  function CustomGamesController($state) {
    var vm = this;
    
    var recentServersMockList = [
        {
          'id': 1,
          'name': "Heresy",
          'players': 'Rvach, Andrew, Player1, Player2' 
        },
        {
          'id': 2,
          'name': "Boduhar",
          'players': 'Player1, Player2, Player3' 
        },
        {
          'id': 3,
          'name': "KarToP/\9",
          'players': 'Bulba1, Bulba2, Bulba3' 
        },
        {
          'id': 4,
          'name': "Warp",
          'players': 'Creature1, Creature2, Creature3' 
        }
      ]; 
    
    vm.recentServers = function() {
      return recentServersMockList;
    }
    
    vm.Create = function() {
      $state.go('base.games.custom.create');
    }   
    
    vm.Join = function() {
      $state.go('base.games.custom.join');
    }  
    
    vm.Watch = function() {
      alert('Currently not implemented...');
      $state.go('base.games.custom');
    }
  }
