# Prerequisites
  Place this folder to any subfolder of nw.js (e.g. c:\\nwjs-v0.18.4-win-x64\tau)
# Building
  npm install
  bower install
  run build.bat and wait until building is finished
# Running Launcher
  Execute nw.exe with parameter 'tau/dist' like this:

    nw.exe tau/dist

# Updating
    Rewrite https://github.com/Rvach/nw-auto-updater for launcer update purposes
        Check for updates
        Download & Unpack
        Restart app with chrome.runtime.reload() or smth like that

# Updating Entire Launcher (with nw.js itself)
        Its possible to have two directories for launchers: 'place1' and 'place2'.
            Unpack into 'place2' directory
            Clear 'place1' directory
            Start updated launcher from 'place2' directory and cleanup 'place1'.
            And so on...


# Temporary Sollutions

maps.service.js
>  ~~Hack for case when all bsp packed // TO DO implement read of .pk3 archives~~ maps.json now represents content of .pk3 files

# TODO
   1) proper registration with checks
   2) ~~config editor with load/save functionality~~
   3) irc chatting
   4) more complicated updater to enable partial updates 
